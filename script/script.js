/*Завдання 1.
*
* 1. Цикл потрібні для того, щоб  була можливість виконувати одну й ту саму дію декілька разів не дублючи код*

   2. Цикл while використовуємо коли не знаємо точну кількість разів, скільки потрібно повторити дію
*  Цикл for використовуємо коли поторити дію потрібно точну кількість разів
* Цикл do..while використовуємо, коли перед перевіркою умови циклу, дію потрібно виконати хоча б один раз.
*
* 3. Явне перетворення типів відбувається коли ми застосовуємо до типу даних метод перетворення на інший тип даних.
* Число 5=> String(5) => '5' тип даних рядок.
* НЕявні перетворення відбуваються під час виконання дій окремих операторів над операндами. В умові if (5 > 6)
* після перевірки умова в дужках повертає true або false.
* */

/*Exercise 2*/
 let userNumber = +prompt('Enter your number');

 while (!userNumber || isNaN(userNumber) || userNumber <= -1 || userNumber === 'null' || (!/\S/.test(userNumber)) || !Number.isInteger(userNumber)) {
   userNumber = +prompt('Error. Enter your number one more time');
 }
    if (userNumber < 5) {
        console.log("Sorry, there are no proper numbers");
    }
        for (let i = 0; i <= userNumber; i++) {
            if (i % 5 === 0) {
                console.log(i);
            }
        } /*Exercise 3*/

/*
let mNumber = +prompt('Enter number m');


while (!mNumber || isNaN(mNumber) || mNumber <= -1 || mNumber === 'null' || (!/\S/.test(mNumber)) || !Number.isInteger(mNumber)) {
     mNumber = +prompt('Enter number m');
}

let nNumber = +prompt('Enter number n');

while (!nNumber || isNaN(nNumber) || nNumber <= -1 || nNumber === 'null' || (!/\S/.test(nNumber)) || !Number.isInteger(nNumber)) {
    nNumber = +prompt('Enter number n');
}

while (mNumber > nNumber) {
    mNumber = +prompt('m should be smaller than n. Please,enter number m');
    nNumber = +prompt('n should be bigger than m. Please, enter number n');
}

 for (let i = mNumber; i <= nNumber; i++){
      if( i > 1) {
          let isPrime = true;
          for (let j = 2; j < i; j++) {
              if (i % j === 0) {
                  isPrime = false;
              }
          }
          if (isPrime) {
            console.log(i);
          }
      }
 }


*/
